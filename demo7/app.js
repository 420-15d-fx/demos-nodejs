"use strict";

const http = require("http");

const html = `<html lang="fr">
<head>
<title>Blogue</title>
</head>
<body>
    <h1>Bienvenue sur mon blogue</h1>
</body>
</html>`;

const server = http.createServer((req, res) =>{

	res.writeHead(200, {
		"Content-Type": "text/html; charset=utf-8",
		"Content-Length": Buffer.byteLength(html)
	  });
	res.end(html);

});

server.listen(3000, () => {
	console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
  });