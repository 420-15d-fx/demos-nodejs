"use strict";

const http = require('http');

// Création du serveur
const server = http.createServer();

// Gestionnaire d'événements qui s'exécute lorsqu'une requête (request) est reçu
server.on('request', function(request, response) {
  console.log('request.url ====> ', request.url);
  console.log('request.method ====> ', request.method);
  console.log('request.headers ====> ', request.headers);
  console.log("")
});

// Écoute le port 3000
server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
});