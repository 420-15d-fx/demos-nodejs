"use strict";
const http = require('http');

const server = http.createServer((req, res) => {
  const { method, url } = req;
  // La méthode setHeader() permet de définir un en-tête HTTP
  res.setHeader('Content-Type', 'text/html; charset=utf-8');
  // La méthode write() permet d'envoyer une réponse au client
  res.write('Message du serveur<br>');
  // La méthode end() permet de terminer la réponse et de l'envoyer au client
  // On peut passer en paramètre de la méthode end() une chaîne de caractères
  res.end(`methode : ${method} - url : ${url}`);
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
});