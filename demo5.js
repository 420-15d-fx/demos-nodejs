"use strict";

const http = require('http');


const homePage = `<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Enter Message</title>
</head>
<body>
    <form action="/message" method="POST">
        <input type="text" name="message" required>
        <button type="submit">Send</button>
    </form>
</body>
</html>`;

const successPage = `<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultat</title>
</head>
<body>
    <h2>Merci ! Votre message a bien été reçu.</h2>
    <a href="/">Retour</a>
</body>
</html>`;
const server = http.createServer((req, res) => {
  
  const { method, url } = req;  // Extraction de la méthode et de l'URL de la requête

  // Gestion de la route principale "/"
  if (url === '/' && method === 'GET') {
    // Définition des en-têtes HTTP
    res.setHeader("Content-Type", "text/html; charset=utf-8"); // Spécifie que la réponse est en HTML avec encodage UTF-8
    res.setHeader("Content-Length", Buffer.byteLength(homePage)); // Définit la taille du contenu pour optimiser la transmission

    res.statusCode = 200; // Indique que la requête a réussi (200 OK)

    res.write(homePage); // Écriture de la page HTML dans la réponse
    return res.end(); // Termine la réponse et l'envoie au client

  }

  // Gestion du formulaire envoyé en POST vers "/message"
  if (url === '/message' && method === 'POST') {

    //writeHead est utilisé pour définir les en-têtes et le statut HTTP en une seule étape. Une fois appelé, les en-têtes ne peuvent plus être modifiés.
    res.writeHead(200, {
      "Content-Type": "text/html; charset=utf-8",
      "Content-Length": Buffer.byteLength(successPage)
    });
    return res.end(successPage); // Envoie la page HTML au client
  } 
    
 // Gestion des routes inexistantes (404)
 res.setHeader("Content-Type", "text/plain; charset=utf-8"); // Spécifie que la réponse est du texte brut (plain text)
 res.statusCode = 404; // Définit le statut HTTP 404 (Page non trouvée)
 res.end("Erreur 404 : Page non trouvée"); // Envoie un message d'erreur au client
  
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
});