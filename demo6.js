"use strict";

const http = require('http');


const homePage = `<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Enter Message</title>
</head>
<body>
    <form action="/message" method="POST">
        <input type="text" name="message" required>
        <button type="submit">Send</button>
    </form>
</body>
</html>`;

// Fonction générant la page de confirmation avec le message reçu
const successPage = (message) => `<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultat</title>
</head>
<body>
    <h2>Merci ! Votre message : "${message}" a bien été reçu.</h2>
    <a href="/">Retour</a>
</body>
</html>`;

const server = http.createServer((req, res) => {
  
  const { method, url } = req;  // Extraction de la méthode et de l'URL de la requête

  // Gestion de la route principale "/"
  if (url === '/' && method === 'GET') {
    // Définition des en-têtes HTTP
    res.setHeader("Content-Type", "text/html; charset=utf-8"); // Spécifie que la réponse est en HTML avec encodage UTF-8
    res.setHeader("Content-Length", Buffer.byteLength(homePage)); // Définit la taille du contenu pour optimiser la transmission

    res.statusCode = 200; // Indique que la requête a réussi (200 OK)

    res.write(homePage); // Écriture de la page HTML dans la réponse
    return res.end(); // Termine la réponse et l'envoie au client

  }

  // Gestion du formulaire envoyé en POST vers "/message"
  if (url === '/message' && method === 'POST') {

    let body = ""; // Stockage du corps de la requête

    // Événement qui se déclenche lorsqu'une partie de la requête est reçue
    // data est un événement spécial qui se déclenche lorsqu'une partie de la requête est reçue
    req.on('data', (chunk) => {
        console.log('chunk',chunk);
        //console.log('string', chunk.toString());
        body += chunk;
      });

    // Événement qui se déclenche lorsque la requête est terminée
    req.on('end', () => {
     
        //console.log('body', body);
        // Transformer la chaîne en un objet utilisable avec URLSearchParams
        const params = new URLSearchParams(body);
        //console.log('params', params)
            
        // Extraire les valeurs
        const message = params.get("message");
        const responseHtml = successPage(message); // Génération de la page de confirmation avec le message reçu
        res.writeHead(200, {
            "Content-Type": "text/html; charset=utf-8",
            "Content-Length": Buffer.byteLength(responseHtml)
          });
        return res.end(responseHtml);
      });

      return;
  } 
    
 // Gestion des routes inexistantes (404)
 res.setHeader("Content-Type", "text/plain; charset=utf-8"); // Spécifie que la réponse est du texte brut (plain text)
 res.statusCode = 404; // Définit le statut HTTP 404 (Page non trouvée)
 res.end("Erreur 404 : Page non trouvée"); // Envoie un message d'erreur au client
  
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
});