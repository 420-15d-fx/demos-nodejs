"use strict";

const http = require('http');
const routes = require('./routes');
// import routes from 'routes';

const server = http.createServer(routes);
// const server = http.createServer(routes.handler);
// console.log('texte: ', routes.texte);
server.listen(3000, () => {
	console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
  });