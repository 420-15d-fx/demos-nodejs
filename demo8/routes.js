"use strict";

const requestHandler = (req, res) => {

  const homePage = `<!DOCTYPE html>
  <html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Enter Message</title>
  </head>
  <body>
      <form action="/message" method="POST">
          <input type="text" name="message" required>
          <button type="submit">Send</button>
      </form>
  </body>
  </html>`;
  
  const successPage = `<!DOCTYPE html>
  <html lang="fr">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Résultat</title>
  </head>
  <body>
      <h2>Merci ! Votre message a bien été reçu.</h2>
      <a href="/">Retour</a>
  </body>
  </html>`;

  const url = req.url;
  const method = req.method;

  if (url === '/' && method === 'GET') {
    res.writeHead(200, {
      "Content-Type": "text/html; charset=utf-8",
      "Content-Length": Buffer.byteLength(homePage)
    });
    return res.end(homePage);
  }
  if (url === '/message' && method === 'POST') {
    res.writeHead(200, {
      "Content-Type": "text/html; charset=utf-8",
      "Content-Length": Buffer.byteLength(successPage)
    });
    return res.end(successPage);
  }

};

module.exports = requestHandler;

